import {User} from './user';
import {Car} from './car';

export interface Travel {
  _id?: string;
  departure?: string;
  destination?: string;
  departureAddress?: string;
  destinationAddress?: string;
  price?: number;
  date?: Date;
  placesAvailable?: number;
  averageTime?: number;
  distance?: number;
  isOnline?: boolean;
  car?: Car;
  user?: User;
  passengers?: User[];
  comments?: [{
    commentBody: string,
    rating: number,
    commentUser: User,
    commentDate: number
  }];
}
