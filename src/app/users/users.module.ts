import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {TravelService} from '../travels/services/travel.service';
import {AuthGuard} from '../guards/auth.guard';
import {AuthService} from './services/auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng2OrderModule} from 'ng2-order-pipe';
import {Ng2SearchPipeModule} from 'ng2-search-filter/index';
import {FlashMessagesModule, FlashMessagesService} from 'angular2-flash-messages';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {JwtModule} from '@auth0/angular-jwt';
import {NgxPaginationModule} from 'ngx-pagination';
import {Ng4GeoautocompleteModule} from 'ng4-geoautocomplete';
import {RouterModule} from '@angular/router';
import { UsersListComponent } from './components/users-list/users-list.component';
import {UserService} from './services/user.service';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('x-auth');
        },
        headerName: 'x-auth',
        authScheme: '',
        whitelistedDomains: ['localhost:5000']
      }
    }),
    FlashMessagesModule,
    NgxPaginationModule,
    Ng4GeoautocompleteModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2OrderModule,
    RouterModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    UsersListComponent,
    UserProfileComponent,
    ChangePasswordComponent],
  exports: [
    LoginComponent,
    RegisterComponent,
    UsersListComponent,
    UserProfileComponent,
    ChangePasswordComponent
  ],
  providers: [
    AuthService,
    TravelService,
    AuthGuard,
    UserService,
    FlashMessagesService
  ]
})
export class UsersModule {
}
