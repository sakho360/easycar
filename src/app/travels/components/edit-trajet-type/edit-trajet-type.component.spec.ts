import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditTrajetTypeComponent} from './edit-trajet-type.component';

describe('EditTrajetTypeComponent', () => {
  let component: EditTrajetTypeComponent;
  let fixture: ComponentFixture<EditTrajetTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditTrajetTypeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTrajetTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
