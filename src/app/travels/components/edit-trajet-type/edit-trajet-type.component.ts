import {Component, OnInit} from '@angular/core';
import {TravelType} from '../../../models/travelType';
import {TravelService} from '../../services/travel.service';
import {Router, ActivatedRoute} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';

declare let google;

@Component({
  selector: 'app-edit-trajet-type',
  templateUrl: 'edit-trajet-type.component.html',
  styleUrls: ['edit-trajet-type.component.css']
})
export class EditTrajetTypeComponent implements OnInit {

  travelType: TravelType = {
    price: 0,
    distanceMax: 0,
  };
  id: string;


  constructor(private travelService: TravelService,
              private router: Router,
              private route: ActivatedRoute,
              private flashMsgService: FlashMessagesService) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.travelService.getTravelType(this.id).subscribe(res => {
      console.log(res.travelType);
      this.travelType = res.travelType;
    });
    console.log(this.travelType);
  }



  editTravelType({value, valid}: { value: TravelType, valid: boolean }) {
    if (valid) {
      this.travelService.editTravelType(this.id, value).subscribe(
        (res) => {
          this.flashMsgService.show('Trajet Type modifié', {
            cssClass: 'alert alert-success', timeout: 4000
          });
          this.router.navigate(['/travel-types']);
        },
        (err) => {
          this.flashMsgService.show(err.error.message, {
            cssClass: 'alert alert-danger', timeout: 4000
          });
          this.router.navigate(['/dashboard']);
        });
    } else {
      console.log(value);
    }
  }

}
